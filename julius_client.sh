#!/bin/sh

gnome-terminal --geometry=80x30+0-0 \
--tab --active --title=smallvocab_client -e "$HOME/julius/smallvocab_client.sh" \
--tab --title=typewriter_client -e "$HOME/julius/typewriter_client.sh" \
--tab --title=epd_client -e "$HOME/julius/epd_client.sh" \
--tab --title=phoneme_compare_clinet -e "$HOME/julius/phoneme_compare_client.sh" \
&

# coding:utf-8
#!/usr/bin/env python
from __future__ import print_function
import socket
from contextlib import closing
import xml.etree.ElementTree as ET
import sys
import signal
import time
import re
import threading

#Ctrl+C de anzen ni teisi
def sighandler(event, signr, handler):
    event.set()
    sock.close()
    sock2.close()

def ChangeLex(sock,conn,bufsize,event):
    while not event.is_set():
        conn.recv(bufsize)
        new_gram = "smallvocab2"
        cmd = "CHANGEGRAM " + new_gram + "\n"
        dfa = open("/home/moriya/julius/grammar-kit-v4.1/smallvocab/smallvocab.dfa")
        dic = open("/home/moriya/julius/grammar-kit-v4.1/smallvocab/smallvocab.dict")
        dfadata = dfa.read()
        dicdata = dic.read()
        dfa.close()
        dic.close()
        sock.send(cmd)
        sock.send(dfadata + "\n")
        sock.send("DFAEND\n")
        sock.send(dicdata+ "\n")
        sock.send("DICEND\n")


def msg_split(data):
    cmd = []

    data = ""
    SCORE = 0
    AMSCORE = 0
    while(1):
        if(data.find("<INPUT STATUS=\"LISTEN\"")):
            #sys.stderr.write("macth junk")
            data = re.sub(r"<INPUT STATUS=\"LISTEN\" \"\d+\"/>", "", data)
        data = data.replace("\n.","")
        if "</RECOGOUT>" in data:
            if(data.find("<INPUT")):
                data = re.sub(r"<INPUT[\w]","",data)
            sys.stderr.write(data)
            f = open("smallvocab.log","w")
            f.write('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):])
            f.close()
            root = ET.fromstring('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):].replace("</s>","&lt;/s&gt;").replace("<s>","&lt;s&gt;").replace("\n.", "").replace(".",""))
            for score in root.findall("./SHYPO"):
                SCORE = float(score.get("SCORE"))
            #    AMSCORE =("acoustic=" + score.get("AMSCORE"))
            for whypo in root.findall("./SHYPO/WHYPO"):
                if(whypo.get("WORD") != "<s>" and whypo.get("WORD") != "</s>"):
                    cmd.append(whypo.get("WORD").replace(" ","/"))
                    #print(whypo.get("PHONE"))
            data = ""
            break
        else:
            data = data + sock.recv(1024)

    SCORE = ACOUSTIC = SCORE / 2.0
    cmd = "/".join(cmd)
    cmd = "comment=START-OF-FILE\n\nUTTERANCE=1\n\nNBEST=1\n\nORDER=1 WORDS=UTT-START/" + cmd + "/UTT-END/ score=" + str(SCORE) + " acoustic=" + str(ACOUSTIC) + "\n\ncomment=END-OF-FILE"
    conn.send(cmd)
    print(cmd)

def main(sock,event):

    while not event.is_set():
        recv_data = sock.recv(bufsize)


        #sys.stderr.write(recv_data)
        #print(conn.recv(bufsize))
        msg_split(recv_data)
        time.sleep(10)

if __name__=="__main__":
    e = threading.Event()
    signal.signal(signal.SIGINT, (lambda a, b: sighandler(e, a, b)))
    global sock
    global sock2
    host = "localhost"
    port = 10500
    port2 = 6005
    backlog = 10
    bufsize = 4096
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host,port))
    sock2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock2.bind((host,port2))
    sock2.listen(backlog)
    count = 0
    conn,address = sock2.accept()
    """
    thread1 = threading.Thread(target = main,args = (sock,e))
    thread2 = threading.Thread(target = ChangeLex,args = (sock,conn,bufsize,e))
    thread1.start()
    thread2.start()
    while thread1.isAlive():
        time.sleep(0.5)"""

    main(sock,e)

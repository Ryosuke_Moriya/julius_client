#coding:utf-8
#/usr/bin/env python

import subprocess


def KillProcess(cmd, args, processName, output):
    for i in range(len(output)):
        if(output[i].find(processName) != -1):
            out_split = output[i].split(" ")
            subprocess.call([cmd, args, out_split[4]])

if __name__ == "__main__":
    PID = []
    cmd = "kill"
    args = "-9"
    output = subprocess.check_output(["ps","-ux"]).split("\n")
    KillProcess(cmd, args, "julius -C", output)
    KillProcess(cmd, args, "python smallvocab_client", output)
    KillProcess(cmd, args, "python typewriter_client", output)
    KillProcess(cmd, args, "python epd_client", output)
    KillProcess(cmd, args, "python phoneme_client", output)

"""
for i in range(len(output)):
    if(output[i].find("julius -C") != -1):
        out_split = output[i].split(" ")
        subprocess.call([cmd,out_split[4]])
    elif(output[i].find("python smallvocab_client") != -1):
        out_split = output[i].split(" ")
        subprocess.call([cmd,out_split[4]])
    elif(output[i].find("python typewriter_client") != -1):
        out_split = output[i].split(" ")
        subprocess.call([cmd,out_split[4]])
    elif(output[i].find("python epd_client") != -1):
        out_split = output[i].split(" ")
        subprocess.call([cmd,out_split[4]])
    elif(output[i].find("python phoneme_client") != -1):
        out_split = output[i].split(" ")
        subprocess.call([cmd,out_split[4]])
"""

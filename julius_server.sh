#!/bin/sh

gnome-terminal --geometry=80x30+0-0 \
--tab --active --title=smallvocab -e "$HOME/julius/smallvocab_server.sh" \
--tab --title=typewriter -e "$HOME/julius/typewriter_server.sh" \
--tab --title=epd -e "$HOME/julius/epd_server.sh" \
--tab --title=phoneme_compare -e "$HOME/julius/phoneme_compare_server.sh" \
&

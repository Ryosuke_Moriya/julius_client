#coding:utf-8
#!/usr/bin/env python
from __future__ import print_function
import socket
from contextlib import closing
import xml.etree.ElementTree as ET
import sys
from signal import signal, SIGPIPE, SIG_DFL, SIGINT, SIGTERM
import time
#from scipy.io.wavfile import read
import os
from glob import glob
import threading

#Ctrl+C de anzen ni teisi
def stop_handler(signum, frame):
    print("\nget signal ctrl+C")
    sock.close()
    sock2.close()
    time.sleep(5)
    sys.exit()

def get_latest_filepath(dirname):
    target = os.path.join(dirname,"*")
    files = [(f,os.path.getmtime(f)) for f in glob(target)]
    latest_filepath = sorted(files, key = lambda files: files[1])[-1]
    return latest_filepath[0]

"""
def recv_ptmove(conn2,bufsize):
    global epd_slot
    epd_slot = conn2.recv(bufsize)
    print(epd_slot)
    return epd_slot
"""
"""
def recv_julius(sock,bufsize):
    while(1):
        print(recv_data = sock.recv(bufsize))
        if("<INPUT STATUS=\"STARTREC\"" in recv_data):
            startflag = True
            endflag = False
        if("<INPUT STATUS=\"ENDREC\"" in recv_data):
            endflag = True
            startflag = False
"""

def main(sock,conn2,bufsize):
    dirname = "/home/moriya/julius/wav"
    start = "BEGIN\n\n"
    end = "END\n\n"
    epd_slot = conn2.recv(bufsize)
    #count = 0
    while(1):
        if(epd_slot == "epdon\n"):
            recv_data = sock.recv(bufsize)
            if("<INPUT STATUS=\"STARTREC\"" in recv_data):
                print(recv_data)
                #conn.send(start)
                conn2.send(start)
                print("send start")
                #print(conn.recv(bufsize))
                #print(recv_data)
                #print(conn.recv(bufsize))
                
            else:
                pass

if __name__ == "__main__":
    signal(SIGPIPE,SIG_DFL)
    signal(SIGTERM, stop_handler)
    signal(SIGINT, stop_handler)
    host = "localhost"
    port = 10502
    port2 = 5002
    backlog = 10
    bufsize = 4096
    global sock
    global sock2
    global epd_slot
    global recv_data
    global endflag
    global startflag
    startflag = None
    endflag = None
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.connect((host,port))
    sock2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock2.bind((host,port2))
    sock2.listen(backlog)
    #conn,address = sock2.accept()
    conn2,address2 = sock2.accept()
    #thread_main = threading.Thread(target = main,args = (sock,conn,conn2,bufsize))
    #thread_recv = threading.Thread(target = recv_ptmove,args = (conn2,bufsize))
    #thread_recvjulius = threading.Thread(target = recv_julius,args = (sock,bufsize))
    #thread_main.start()
    #thread_recv.start()
    #thread_recvjulius.start()
    main(sock,conn2,bufsize)

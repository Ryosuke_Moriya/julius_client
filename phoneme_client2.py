#coding:utf-8
#!usr/bin/env python
from __future__ import print_function
import socket
from contextlib import closing
import xml.etree.ElementTree as ET
import sys
import signal
import time
import re
from multiprocessing import Process, Queue, Event

SLEEP_SEC = 1

class ChangeLex(object):
    def __init__(self):
        new_gram = "smallvocab2"
        self.cmd = "CHANGEGRAM " + new_gram + "\n"
        dfa = open("/home/moriya/julius/grammar-kit-v4.1/phoneme_compare/phoneme.dfa")
        dic = open("/home/moriya/julius/grammar-kit-v4.1/phoneme_compare/phoneme.dict")
        self.dfadata = dfa.read()
        self.dicdata = dic.read()
        dfa.close()
        dic.close()

    def run(self,sock,conn,bufsize,stop_flag):

        signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGTERM, signal.SIG_IGN)

        print("start change lex\n")

        while True:
            if stop_flag.is_set():
                break

            conn.recv(bufsize)
            sock.send(self.cmd)
            sock.send(self.dfadata + "\n")
            sock.send("DFAEND\n")
            sock.send(self.dicdata + "\n")
            sock.send("DICEND")

        print ("stop change lex\n")

class SpeechRecog(object):
    def __init__(self):
        self.init = "init"

    def run(self,sock,stop_flag):

        signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGTERM, signal.SIG_IGN)

        print("start SpeechRecog\n")

        while True:
            if stop_flag.is_set():
                break

            recv_data = sock.recv(bufsize)
            self.msg_split(recv_data)
            time.sleep(10)

        print("stop SpeechRecog")

    def msg_split(self,data):
        cmd = []
        data = ""
        SCORE = 0
        while(1):
            if(data.find("<INPUT STATUS=\"LISTEN\"")):
                data = re.sub(r"<INPUT STATUS=\"LISTEN\"[\w/:%#\$&\?\(\)~\.=\+\-…\s]+", "", data)
            data = data.replace("\n.","")
            if "</RECOGOUT>" in data:
                if(data.find("<INPUT")):
                    data = re.sub(r"<INPUT[\w]","",data)
                sys.stderr.write(data)
                f = open("smallvocab.log","w")
                f.write('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):])
                f.close()
                root = ET.fromstring('<?xml version="1.0" encoding="UTF-8"?>\n' + data[data.find("<RECOGOUT>"):].replace("</s>","&lt;/s&gt;").replace("<s>","&lt;s&gt;").replace("\n.", "").replace(".",""))
                for score in root.findall("./SHYPO"):
                    SCORE = float(score.get("SCORE"))
                #    AMSCORE =("acoustic=" + score.get("AMSCORE"))
                for whypo in root.findall("./SHYPO/WHYPO"):
                    if(whypo.get("WORD") != "<s>" and whypo.get("WORD") != "</s>"):
                        cmd.append(whypo.get("WORD").replace(" ","/"))
                        #print(whypo.get("PHONE"))
                data = ""
                break
            else:
                data = data + sock.recv(1024)

        SCORE = ACOUSTIC = SCORE / 2.0
        cmd = "/".join(cmd)
        cmd = "comment=START-OF-FILE\n\nUTTERANCE=1\n\nNBEST=1\n\nORDER=1 WORDS=UTT-START/" + cmd + "/UTT-END/ score=" + str(SCORE) + " acoustic=" + str(ACOUSTIC) + "\n\ncomment=END-OF-FILE"
        conn.send(cmd)
        print(cmd)

if __name__ == "__main__":

    stop_flag = Event()

    sub1 = ChangeLex()
    sub2 = SpeechRecog()

    host = "localhost"
    port = 10503
    port2 = 6007
    backlog = 10
    bufsize = 4096
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host,port))
    sock2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock2.bind((host,port2))
    sock2.listen(backlog)
    conn,address = sock2.accept()

    sub1_process = Process(target = sub1.run, args = (sock,conn,bufsize,stop_flag))
    sub2_process = Process(target = sub2.run, args = (sock,stop_flag))

    processes = [sub1_process,sub2_process]
    for p in processes:
        p.start()

    def signalHandler(signal, handler):
        stop_flag.set()

    signal.signal(signal.SIGINT, signalHandler)
    signal.signal(signal.SIGTERM, signalHandler)

    while True:
        alive_flag = False
        for p in processes:
            if p.is_alive():
                alive_flag = True
                break
        if alive_flag:
            time.sleep(0.1)
            continue
        break
